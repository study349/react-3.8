//3.8.1 ======================================================
function concat<string>(arg1: string, arg2: string): string{
  return arg1 + arg2;
} 

const val = concat("Hello ", "World");
console.log(val);



//3.8.2 ======================================================
interface IHometask{
    howIDoIt: string;
    simeArray: Array<string | number>;
    withData: [{ howIDoIt, simeArray }];
}

const MyHometask: IHometask = {
    howIDoIt: "I Do It Wel",
    simeArray: ["string one", "string two", 42],
    withData: [{ howIDoIt: "I Do It Wel", simeArray: ["string one", 23] }],
}

console.log(MyHometask);



//3.8.3 ======================================================
interface MyArray<T> {
    [N: number]: T;
 
    reduce<U>(fn: (accumulator: U, value: T) => U, initValue?: T | number): U;
}


const myAr: MyArray<number> = [1,2,3,4,5];

let initVal = 10;
console.log(myAr);
console.log(myAr.reduce((f, val) => f + val, initVal));



//3.8.4 ======================================================
interface IHomeTask {
    data: string;
    numbericData: number;
    date: Date;
    externalData: {
        basis: number;
        value: string;
    }
}


type MyPartial<T> = {
    [N in keyof T]?: T[N] extends object ? MyPartial<T[N]> : T[N]
}


const homeTask: MyPartial<IHomeTask> = {
    externalData: {
        value: 'win'
    }
}



console.log(homeTask.externalData.value);